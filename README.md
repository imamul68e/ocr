#Optical Character Recognition with Tesseract and Java


Step 1: Open a Java or spring boot project

Step 2: Add the following tess4j dependency in pom.xml
	
	<dependency>
    	<groupId>net.sourceforge.tess4j</groupId>
    	<artifactId>tess4j</artifactId>
    	<version>4.5.1</version>
	</dependency>

Step 3: Create a java class and method. In that method paste the followings:
	
	File image = new File("<..file path>/img.png");
	Tesseract tesseract = new Tesseract();
	tesseract.setDatapath("src/main/resources/tessdata");
	tesseract.setLanguage("eng");
	tesseract.setPageSegMode(1);
	tesseract.setOcrEngineMode(1);
	String result = tesseract.doOCR(image);

Step 4: Download osd.traineddata and eng.traineddata files from git and keep these at src/main/resources/tessdata of the project. This tessdata directory can be other directory.

N.B: Tesseract OCR support for various image formats like JPEG, GIF, PNG, and BMP.
