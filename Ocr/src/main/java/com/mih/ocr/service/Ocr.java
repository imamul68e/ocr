package com.mih.ocr.service;

import java.io.File;

import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

public class Ocr {

	/* OCR support for various image formats like JPEG, GIF, PNG, and BMP. */
	/*
	 * https://www.baeldung.com/java-ocr-tesseract
	 */
	public static void main(String[] args) {
		//File image = new File("D:\\Documents\\shafikul\\MD.pdf");
		File image = new File("C:\\Users\\Imamul Hossain\\Pictures\\300.png");
		Tesseract tesseract = OcrUtils.getUtils();
		try {
			String result = tesseract.doOCR(image);
			System.out.println("Output is " + result);

		} catch (TesseractException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
