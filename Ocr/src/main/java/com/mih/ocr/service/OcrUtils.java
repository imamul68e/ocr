package com.mih.ocr.service;

import net.sourceforge.tess4j.Tesseract;

public class OcrUtils {
	public static Tesseract getUtils() {
		Tesseract tesseract = new Tesseract();
		tesseract.setDatapath("src/main/resources/tessdata");
		tesseract.setLanguage("eng");
		tesseract.setPageSegMode(1);
		tesseract.setOcrEngineMode(1);

		return tesseract;
	}
}
